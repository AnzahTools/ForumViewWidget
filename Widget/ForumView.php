<?php
namespace AnzahTools\ForumViewWidget\Widget;

use XF\Widget\AbstractWidget;

/**
 * Class ForumViewWidget
 *
 * @package AnzahTools\ForumViewWidget\Widget
 */
class ForumView extends AbstractWidget
{
	protected $defaultOptions = [
	];

	public function render()
	{
		$visitor = \XF::visitor();

		$nodeRepo = $this->getNodeRepo();
		$nodes = $nodeRepo->getNodeList();

		$nodeTree = $nodeRepo->createNodeTree($nodes);
		$nodeTree = $nodeTree->filter(null, function($id, \XF\Entity\Node $node, $depth, $children, \XF\Tree $tree)
		{
			if ($children)
			{
				return true;
			}
			if ($node->node_type_id == 'Forum')
			{
				return true;
			}
			return false;
		});

		$nodeExtras = $nodeRepo->getNodeListExtras($nodeTree);

		$viewParams = [
			'nodeTree' => $nodeTree,
			'nodeExtras' => $nodeExtras
		];
		return $this->renderer('at_fvw_widget',$viewParams);
	}

	protected function getNodeRepo()
	{
		return $this->repository('XF:Node');
	}

	public function verifyOptions(\XF\Http\Request $request, array &$options, &$error = null)
	{
		$options = $request->filter([
		]);
		return true;
	}
}
